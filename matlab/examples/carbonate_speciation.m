close all; clear;

addpath ..

% system of equations for carbon speciation
system     = {  '-pKco2 + pH2CO3-pPco2';
                '-pK1 + pHCO3+pH-pH2CO3';
                '-pK2 + pCO3+pH-pHCO3';
                '-pKw + pH+pOH';
                '-10^pCO2T + 10^pH2CO3 +10^pHCO3 + 10^pCO3'};      

% identify variables
keep = {'pH'};

% parameters
p= struct('pKw', -14, 'pKco2', -1.46, 'pK1', -6.35,'pK2', -10.33,'pCO2T', -3);

% generate the solver handle using text2solver
[q, info] = text2solver(system,'Variables', keep,'Parameters',p);

% what values of the variables to sweep
n = 1000;

pH = linspace(-14,0,n)';

% generate an intial guess for the system
x0 = -5*ones(numel(system),n);

% evaluate the function handle at a point of interest
tic;
answer = q(x0, pH);
toc

% plot 
figure; hold on;
for i = 1:numel(system)
    plot(-pH, answer(:,i))
end
plot(-pH, pH)
xlabel('pH');ylabel('log_{10} C');

