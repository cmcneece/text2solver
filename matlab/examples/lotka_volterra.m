close all; clear;

addpath ..

% system of equations for Lotka?Volterra
system     = {'-(u-uo)/dt + p*u';
              '-(v-vo)/dt + s*v';
              '-p + a*u - d*u*v';
              '-s - b*v + g*u*v'};      

% keep previos time step as an input
keep = {'vo','uo'};

% parameters taken from wiki
p= struct('dt',3e-3, 'a', 2/3, 'b', 4/3, 'd', 1, 'g', 1);

% hide the iterations
options = optimoptions('fsolve','Display', 'final','Jacobian', 'on');

% generate the solver handle
[q, s] = text2solver(system,'Variables', keep,'Parameters', p,'solveroptions', options);

% intiial values for pred and prey
n = 5;
u = 1.5*ones(n,1);
v = linspace(1,2,n)';

% intial guess for the system
a{1} = ones(numel(system),length(u));

% final time
tf = 10;

% step through time
ind = 2;
t = p.dt:p.dt:8;

for i = t
    a{ind} = q(a{ind-1}, [v(:,ind-1) u(:,ind-1)])';
    u(:,ind) = a{ind}(s.sub.u,:);
    v(:,ind) = a{ind}(s.sub.v,:);
    ind = ind+1;
end

% plot 
figure; hold on;
for i=1:size(u,1)
    plot(u(i,1), v(i,1), 'ok','MarkerFaceColor', 'k');
    plot(u(i,:),v(i,:));
end
xlim([0 4]);
ylim([0 2.5])
ylabel('v'); xlabel('u');