close all; clear;

addpath ..
% system of equations for Lotka?Volterra
system     = {'-(x - xo)/dt + s*(y - x)';
              '-(y - yo)/dt + x*(p - z) - y';
              '-(z - zo)/dt + x*y - b*z'};      

% keep previos time step as an input
keep = {'xo','yo','zo'};

% parameters taken from wiki
p= struct('dt',1e-2, 'p', 28, 's', 10,'b', 8/3);

% hide the iterations
options = optimoptions('fsolve','Display', 'off','Jacobian', 'on');

% generate the solver handle
[q, info] = text2solver(system,'Variables', keep,'Parameters', p,'solveroptions', options);

% intiial values for pred and prey
x = 1;y = -10;z = 20;

% intial guess for the system
a{1} = ones(numel(system),length(x));

% final time
tf = 5;

% step through time
ind = 2;
t = p.dt:p.dt:tf;
figure;
for i = t
    a{ind} = q(a{ind-1}, [x(:,ind-1) y(:,ind-1) z(:,ind-1)])';
    x(:,ind) = a{ind}(info.sub.x,:);
    y(:,ind) =a{ind}(info.sub.y,:);
    z(:,ind) =a{ind}(info.sub.z,:);
    ind = ind+1;
    plot3(x(:),y(:),z(:));
    drawnow;
end

% plot 
figure; hold on;
for i=1:size(x,1)
    plot3(x(i,:),y(i,:),z(i,:));
end

