close all; clear all;

%% create or load a cell of strings for the system of equations
system     = {	'exp(-exp(-(x+y))) - y*(1+x^2)';
                'x*cos(y) + y*sin(x) - A'};

% variable to vary
keep = {'A'};

%% generate the solver handle using text2solver
tic
[q, info] = text2solver(system,'Variables', keep);
toc
%% evaulate the equations

sweep = (0:0.001:2)';
 
% generate an intial guess for the 
x0 = ones(numel(system),numel(sweep));

% evaluate the function handle at a point of interest
tic;
answer = q(x0,sweep);
toc

figure; hold on;
for i = 1:2
    plot(sweep, answer(:,i))
end