# text2solver #

The function text2solver converts a cell array of strings representing the residual of a system of equations into the appropriate form for gradient based numerical solvers and minimization routines. The function uses MATLAB's symbolic toolbox to analytically calculate the jacobian or hessian depending on the numerical routine. 

By default the system is solved using MATLAB's fsolve function, however the user can specify any solver/minimization routine within MATLAB, or use the residual and jacobian function handles to solve the system with custom routines. 

Tools within the program such as parameter subsititution, and "sweep" variables allows the rapid solution of aribitrarily complex systems with minimal knowledge of the function behavior.

## repo contents ##

* text2solver.m - main program file
* examples
    * simple_nonlinear_function.m - a simple example to get started
    * carbonate_speciation.m - equilibrium speciation of carbonate in a closed system
    * lorenz.m - lorenz attractor example
    * lotka_volterra.m - pred/prey example, another dynamic system

## questions? ##

contact Colin McNeece, colinjmcneece at gmail.com