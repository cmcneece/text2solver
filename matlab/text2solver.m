function [q, in] = text2solver(system, varargin)
%   text2solver(system, varargin) accepts a cell array of strings which
%   represent the residual of a system of nonlinear equations. The function
%   calculates the analytical Jacobian and Hessian, the system is then
%   solved by a gradient based method of the users choice.
%
%   [q,in] = text2solver(system) converts 'system', a cell array of strings,
%   into a function handle q(xo) which can be used to solve the system of
%   equations given an intial guess xo having size (number of unknowns) x 1.
%   'info' is a structure which contains the function handles of the
%   residual, jacobian and hessian, as well as the column location of the 
%   unknowns of the system returned. q(xo) returns a vector 
%   which is 1 x number of unknowns. See s for the appropriate position of 
%   each guess in the guess vector xo.
%
%   Example:
%         system ={'x^2 + y^2 - 10','2*x+y-1'};
%         [q, info] = text2solver(system);
%         xo = [1;1]; 
%         answer = q(xo);
%         y = answer(:,info.sub.y);
%
%   text2solver(system,'parameters',params) accepts a structure of
%   parameter names and values which are substituted into the system of
%   equations. Parameters must be scaler quantities.
%
%   Example:
%         system ={'x^2 + y^2 - 10','2*x+y-L'};
%         param = struct('L', 1);
%         [q, info] = text2solver(system,'parameters',param);
%         xo = [1;1];
%         answer = q(xo);
%         y = answer(:,info.sub.y);
%         x = answer(:,info.sub.x);
%
%   text2solver(system,'variables',vars) accepts an addtional name value 
%   pair which specifies variables which will be kept for later evaluation.
%   The value of the variables must be provided as an additional input as 
%   q(xo, sweep), where sweep is a
%   matrix of size (number of evaluations) by (number of variables), each column
%   corresponding to the value of the variables in vars. xo must then be a matrix
%   of size (number of unknowns) by (number of evaluations), q returns a matrix
%   of size (number of evaluations) by (number of unknowns). The columns of
%   answer correspond to the values of the unknowns for each evaluation in vars.
%
%   Example:
%         system ={'x^2 + y^2 - L','2*x+y-L'};
%         vars = {'L'};
%         [q, info] = text2solver(system,'variables',vars);
%         sweep=(0:0.001:5)';
%         xo = repmat([1;1],1,numel(sweep));
%         answer = q(xo,sweep);
%         y = answer(:,info.sub.y);
%         x = answer(:,info.sub.x);
%         figure;
%         plot(sweep, y, sweep, x)
%
%   text2solver(system,'solveroptions',option) accepts an optimoptions 
%   structure which is passed to the solver. The default options are 
%   optimoptions('fsolve','Display', 'iter','Jacobian','on'). 
%
%   Example:
%         system ={'x^2 + y^2 - L','2*x+y-L'};
%         vars = {'L'};
%         options = optimoptions('fsolve','Display','iter','Jacobian','on','PlotFcn',@optimplotfirstorderopt); 
%         [q, info] = text2solver(system,'variables',vars,'solveroptions',options);
%         sweep=(0:0.001:5)';
%         xo = repmat([1;1],1,numel(sweep));
%         answer = q(xo,sweep);
%         y = answer(:,info.sub.y);
%         x = answer(:,info.sub.x);
%         figure;
%         plot(sweep, y, sweep, x)
%
%   if fminunc is chosen as the solver text2solver converts the system to the 
%   sum of squares form appropriate and computes the analytic Hessian. The 
%   gradient and hessian options must be enabled through optimoptions for 
%   text2solver to compute them:
%
%   Example:
%       options = optimoptions('fminunc','GradObj','on','Hessian','on'); 
%
%   The Jacobian is required for fminunc with the trust-region algorithm,
%   and for fmincon with the trust-region-reflective algorithm. 

% sort out inputs
in = t2sInputParser(system, varargin);   

% create the function handle for the residual, jacobian and hessian
in = functionalize(in);

% solve the system
q = @(xo,varargin) systemsolver(in, xo, varargin);

end

%% functionalize
function [in]	= functionalize(in)

% Turn the cell of strings into a symbolic object
symb	= (sym(in.system));

% substitute the parameters into the system
paramnames = fieldnames(in.parameters)';
paramvalues = struct2cell(in.parameters);

symb = subs(symb, paramnames,[paramvalues{:}]);

% see which symbolic variables remain
Var = symvar(symb);

% remove the user specified variables from the symbolic
if ~isempty(in.variables{1})
    Var = Var(~ismember(Var(:), in.variables(:)));
end

nVars = numel(Var);
nEqns = numel(symb);
names = char(Var);

% check to see if the system is properly defined
assert(nEqns == nVars | nEqns==1,'The chemical system is not properly defined.\nThere are %1.0f unknowns and %1.0f equations.\nThe current variables are: %s',nVars, nEqns,names(10:end-3))

solverName = in.solveroptions.SolverName;
algName = in.solveroptions.Algorithm;

% calculate the hessian matrix if necessary
hessFlag = (strcmpi(solverName,'fminunc') && strcmpi(algName,'trust-region')) || (strcmpi(solverName,'fmincon') && any(strcmpi(algName,{'trust-region-reflective','interior-point'})));
if hessFlag
    symb= sum(symb.^2);
    nEqns = 1;
    hess = hessian(symb,Var);
end

% calculate the jacobian matrix
jacob                   = jacobian(symb,Var);

% create the output structure "s" which shows what substitutions were made
for i = 1:nVars
    k = 0:nVars;
    varStr = char(Var(i));
    count = ['(' num2str(k(i)) '*numel(X)/' num2str(nVars) '+1):(' num2str(k(i+1)) '*numel(X)/' num2str(nVars) ')'];
    subdum.(varStr) = ['X(' count ')'];
    in.sub.(varStr) = i;
end

% Turn the symbolic into a string
inp = 'X';
if ~isempty(in.variables{1})
    for i = 1:numel(in.variables)
        inp = [inp ' , ' char(in.variables{i}), ' '];
    end
end

% create the residual function handle string 
res = ['@(' inp ')['];
for i = 1:numel(symb)
	res = [res '(' char(symb(i)) ');'];
end
res = [res ,']'];

% create the jacobian function handle string
jacob=transpose(jacob);
Jsq = numel(jacob);
jac = ['@(' inp ') ['];
for i = 1:Jsq
    if  any(i==(nEqns:nEqns:Jsq))
        sep = ';';
    else
        sep = ',';
    end
    if nEqns >1
        jac = [jac 'spdiags(((' char(jacob(i)) ').*ones(numel(X)/' num2str(nVars) ',1)),0,numel(X)/' num2str(nVars) ',numel(X)/' num2str(nVars) ')' sep]; 
    else
        jac = [jac '(' char(jacob(i)) ').*ones(numel(X)/' num2str(nVars) ',1)' sep]; 
    end
    
end
jac = [jac ']'];

% create the hessian function handle string 
if hessFlag
    hess=transpose(hess);
    Hsq = numel(hess);
    hes = ['@(' inp ') ['];
    for i = 1:Hsq
        if  any(i==(nVars:nVars:Hsq))
            sep = ';';
        else
            sep = ',';
        end
        hes = [ hes 'spdiags((' char(hess(i)) '*ones(numel(X)/' num2str(nVars) ',1)),0,numel(X)/' num2str(nVars) ',numel(X)/' num2str(nVars) ')' sep];               
    end
    hes = [hes ']'];
end

% sort the names of the substituted variables by length to avoid overwriting variables
names	= fieldnames(subdum);
[~, I]	= sort(cellfun('size', names, 2), 'descend');
names	= names(I);

% replace the variable names by X(#)
for i = 1:nVars
    nameStr = char(names(i));
    strTest = ['(?<=\W)',nameStr,'(?=\W)'];
    res = regexprep(res, strTest, subdum.(nameStr));
    jac = regexprep(jac, strTest, subdum.(nameStr));
    if hessFlag; hes = regexprep(hes, strTest, subdum.(nameStr));end
end

% turn the strings into functions
in.res	= str2func(vectorize(res));
in.jac	= str2func(vectorize(jac));
if hessFlag; in.hes	= str2func(vectorize(hes));end
end

%% solve the system
function Q    = systemsolver(in, xo, varargin)
k = inputParser;

% make sure an initial guess is provided, and the sweep values if necessary
errorGuess = 'Initial guess must be provided and have dimensions (number of unknowns) by (number of evaluations).';
errorSweep = 'If variables are kept for a sweep, values for evaulation must be specified and have dimension (number of evaluations) by (number of swept variables).'; 
guesstest = @(x) assert(isnumeric(x) && size(x,1) == numel(in.system),errorGuess);
sweeptest = @(x) assert(isnumeric(x) && size(x,2) == numel(in.variables),errorSweep);

sweepflag = ~isempty(in.variables{1});
assert(sweepflag == ~isempty(varargin{1}), errorSweep)

addRequired(k, 'xo', guesstest);

if sweepflag
    addRequired(k,'sweep', sweeptest);
    parse(k,xo,varargin{1}{:})
else
    addOptional(k,'sweep', [],sweeptest);
    parse(k,xo)
end

nIn = max(1,size(k.Results.sweep,1));
nEq = numel(in.system);

guessCond = ~any(~(size(k.Results.xo) == [nEq max(1,nIn)]));
assert(guessCond,' Initial guess must have dimensions of (number of equations) by (number of evaluations).');

sweepVec = k.Results.sweep;

% restructure the input and guess for the function handle
input = mat2cell(sweepVec,size(sweepVec,1), ones(1,size(sweepVec,2)));
xo = reshape(xo',numel(xo),1);

if isfield(in, 'hes')
    f = @(x,varargin) wrap(x, sum(in.res(x,varargin{:})), in.jac(x,varargin{:}), in.hes(x,varargin{:}));
else
    f = @(x,varargin) wrap(x, in.res(x,varargin{:}), in.jac(x,varargin{:}));
end

% solve it
solveFun = [in.solveroptions.SolverName,'(@(x) f(x,input{:}), xo , in.solveroptions)'];
Q = eval(solveFun);
Q = reshape(Q,  nIn, numel(in.system));

end


%% function wrapper
function [F,varargout] = wrap(x,F,varargin)
varargout = varargin;
end

%% input parser
function in = t2sInputParser(system, varargin)
p = inputParser;

addRequired(p,'system',@iscell);

defaultsolveroptions = optimoptions('fsolve','Jacobian','on','Display','iter');
defaultparameters = struct();
defaultvariables = {''};
vartest = @(var) iscell(var);
% solvertest = @(x) 
addParameter(p,'parameters',    defaultparameters,      @isstruct);
addParameter(p,'variables',     defaultvariables,       vartest);
addParameter(p,'solveroptions', defaultsolveroptions,   @isobject);

parse(p,system,varargin{1}{:});

in = p.Results;

% remove variables from parameters, in case they overlap
match = {in.variables{isfield(in.parameters,in.variables)}};
in.parameters = rmfield(in.parameters, match);

end