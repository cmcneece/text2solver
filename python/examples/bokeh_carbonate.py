import sys
sys.path.append("..")
from TextToSolver import TextToSolver as tts
import numpy as np
import pdb

from bokeh.io import output_file, show, curdoc
from bokeh.layouts import row, widgetbox, column
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Slider, TextInput
from bokeh.plotting import figure
from bokeh.resources import CDN, INLINE
from bokeh.embed import file_html
import copy

#output_file('bokeh_plot.html')

residual = ['-pg - 1.82*10**6*(e_w*T)**(-3/2)*(sqrt(10**ion)/(1+sqrt(10**ion)) - 0.3*10**ion)',
            '-ion + log(0.5*(4 * 10 ** pCO3 + 10 ** pHCO3 + 10 ** pH + 10 ** pOH))/log(10)',
            '-pK1 + pCO3 + 3*pg + 2 * pH - pCO2',
            '-pK2 + pCO3 + pH + pg - pHCO3',
            '-pKw + pH + pOH + 2*pg',
            '-log(10 ** pCO2T)/log(10) + log(10 ** pCO2 + 10 ** pHCO3 + 10 ** pCO3)/log(10)']

ind_vars = ['pH']
dep_vars = ['pHCO3', 'pCO3', 'pCO2', 'pOH', 'ion', 'pg']
parameters = ['pKw', 'pK1', 'pK2', 'T', 'pCO2T', 'e_w']

pco2t = -3.
temp = 293.
e_w = (87.740 - 0.4008*(temp-273.15) + 9.398e-4*(temp-273.15)**2 -
       1.410e-6*(temp-273.15)**3)
parameters_dict = {'pKw': -14, 'pK1': -16.681,
                   'pK2': -10.33, 'pCO2T': pco2t, 'T': temp,
                   'e_w': e_w}
#delta_H = {'pKw': -14, 'pKco2': -1.46, 'pK1': -5.738,
#              'pK2': -3.561}

options = {'DISPLAY': True}

system = tts(dep_vars, residual, indep_vars=ind_vars,
             parameters=parameters)
n = 100
pH = np.linspace(-14, 0, n)

ind_var = {'pH': pH}

guess = {'pHCO3': -3*np.ones(pH.shape), 'pCO3': -3*np.ones(pH.shape),
        'pCO2': -3*np.ones(pH.shape), 'pOH': -3*np.ones(pH.shape),
        'ion': -1*np.ones(pH.shape), 'pg': -1*np.ones(pH.shape)}
# guess = {var: np.ones(pH.shape) for var in dep_vars}

solution, report = system.solve(guess, indep_var_val=ind_var,
                                input_options=options,
                                parameters=parameters_dict)

solution_plot = copy.deepcopy(solution)
solution_plot['pH'] = pH
source = ColumnDataSource(data=solution_plot)

plot = figure(plot_height=400, plot_width=600, title="carbonate speciation",
              x_range=[-14, 0])
colors = ['red', 'cyan', 'green', 'blue', 'magenta','pink']
for ind, var in enumerate(['pOH', 'pHCO3', 'pCO3', 'pCO2']):
    plot.line('pH', var, source=source, line_color=colors[ind], line_width=3,
              line_alpha=0.6)

plot.xaxis.axis_label = 'log10 [H^+]'
plot.yaxis.axis_label = 'log10 concentration'

# Set up widgets
CO2T = Slider(title="log10 total carbon (M)", value=pco2t, start=-5.0, end=1.0, step=0.1)
T = Slider(title="temperature (K)", value=temp, start=273.0, end=373.0, step=1)

# Set up callbacks
def update_data(attrname, old, new):

    # Get the current slider values
    temp_1 = T.value*np.ones(pH.shape)
    e_w_1 = (87.740 - 0.4008*(temp_1-273.15) + 9.398e-4*(temp_1-273.15)**2 -
           1.410e-6*(temp_1-273.15)**3)
    R = 1.9872036*10**-3
    pK1 = np.log10(np.exp((-5.738/R)*(1/temp_1 - 1/293.))*(10**-16.68))
    pK2 = np.log10(np.exp((-3.561/R)*(1/temp_1 - 1/293.))*(10**-10.33))
    parameters_dict = {'pKw': -14, 'pK1': pK1,
                       'pK2': pK2, 'pCO2T': CO2T.value*np.ones(pH.shape),
                       'T': temp_1, 'e_w': e_w_1}
    new_sol, report = system.solve(solution, indep_var_val=ind_var,
                                   input_options=options, 
                                   parameters=parameters_dict)
    new_sol['pH'] = pH
    source.data = new_sol

CO2T.on_change('value', update_data)
T.on_change('value', update_data)

# Set up layouts and add to document
inputs = widgetbox(CO2T, T)

curdoc().add_root(row(inputs, plot, width=800))
curdoc().title = "bokeh_carbonate"

#show(row(inputs, plot, width=800))
